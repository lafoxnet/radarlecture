package net.lafox.demo.springbootlecture.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Oleg Tsymaenko on 12/3/17.
 */
@Controller
public class IndexController {
    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("name","Вася");
        model.addAttribute("age",17);
        return "index";
    }
}
