package net.lafox.demo.springbootlecture.controller.api;

import net.lafox.demo.springbootlecture.dto.PersonDto;
import net.lafox.demo.springbootlecture.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Oleg Tsymaenko on 12/3/17.
 */

@RestController
public class PersonController {
    @Autowired
    PersonService personService;

    @GetMapping(value = "/api/persons", produces = "application/json")
    public List<PersonDto> getPersons(){
        return personService.getrAllPersons();
    }

    @GetMapping(value = "/api/persons/{id}", produces = "application/json")
    public PersonDto getPersons(@PathVariable Integer id){
        return personService.getPersonById(id);
    }

    @GetMapping(value = "/api/persons/byName/{name}", produces = "application/json")
    public PersonDto getPersonsByName(@PathVariable String name){
        return personService.getPersonByName(name);
    }

    @PostMapping(value = "/api/persons", consumes = "application/json")
    public void postPersons(@RequestBody PersonDto personDto){
        personService.creatrPersons(personDto);
    }
}
