package net.lafox.demo.springbootlecture.repositary;

import net.lafox.demo.springbootlecture.entity.PersonEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Oleg Tsymaenko on 12/4/17.
 */
@Service
public interface PersonRepositary extends CrudRepository<PersonEntity, Integer>{
    PersonEntity findOneByName(String name);
}
