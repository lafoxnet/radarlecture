package net.lafox.demo.springbootlecture.service;

import net.lafox.demo.springbootlecture.dto.PersonDto;
import net.lafox.demo.springbootlecture.entity.PersonEntity;
import net.lafox.demo.springbootlecture.repositary.PersonRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleg Tsymaenko on 12/4/17.
 */

@Service
public class PersonService {
    @Autowired
    private PersonRepositary personRepositary;

    public List<PersonDto> getrAllPersons(){
        List<PersonDto> res = new ArrayList<>();
        for (PersonEntity entity : personRepositary.findAll()) {
            res.add(convertEntityToDto(entity));
        }
        return res;
    }

    private PersonDto convertEntityToDto(PersonEntity entity) {
        PersonDto dto = new PersonDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAge(entity.getAge());
        return dto;
    }

    public void creatrPersons(PersonDto personDto) {
        PersonEntity entity = new PersonEntity();
        entity.setName(personDto.getName());
        entity.setAge(personDto.getAge());
        entity.setId(personDto.getId());
        personRepositary.save(entity);
    }

    public PersonDto getPersonById(Integer id) {
        return convertEntityToDto(personRepositary.findOne(id));
    }

    public PersonDto getPersonByName(String name) {
        return convertEntityToDto(personRepositary.findOneByName(name));
    }
}
